<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        $coinsAvailable = array(100, 50, 20, 10, 5, 2, 1);
        $coinsUsed = array();
        $counter = 0;

        echo "Amount to pay was ".$amount." units.<br><br><br>";
        
        for ($i = 0; $i < count($coinsAvailable); $i++) {
            $value = $coinsAvailable[$i];
            if($amount-$value>=0) {
                $coinsUsed[$counter] = $value;
                $amount -= $value;
                $counter++;
            }    
            if($amount-$value>=0) { //iterate once again with the same coin
                $i--;
            }              
        }
        echo "Payment was done with these coins: ".'<pre>'; print_r($coinsUsed); echo '</pre>';  
        
        $countedCoins = array_count_values($coinsUsed);
        echo "<br>Count the coins!".'<pre>'; print_r($countedCoins); echo '</pre>';
        
        for ($i = 0; $i < count($coinsAvailable); $i++) { //test requires not used coins to be returned, too
            if (!array_key_exists($coinsAvailable[$i], $countedCoins)) {
                $countedCoins[$coinsAvailable[$i]] = 0;
            }    
        }
        echo "<br>Any possible coins which were not used?<br><br>";
        return $countedCoins;
    }
}

$rabbit2 = new WhiteRabbit2();
echo '<pre>'; print_r($rabbit2->findCashPayment(282)); echo '</pre>';