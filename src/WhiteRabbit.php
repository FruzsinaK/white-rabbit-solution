<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);    
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        //TODO implement this!
        $document = file_get_contents($filePath);//returns a string
        $alphas = range('a', 'z');
        $alphasCapital = range('A', 'Z');
        $alphasOcc = array();
        $alphasCapOcc = array();
        
        foreach ($alphas as $value) {
            $alphasOcc[] = substr_count($document, $value); //Counts the number of substring occurrences
        }   
        
        foreach ($alphasCapital as $value) {
            $alphasCapOcc[] = substr_count($document, $value);
        }   

        $sumOfOcc = array(); //Adds letter and capital letter occurrences together
        foreach (array_keys($alphasOcc + $alphasCapOcc) as $key) {
            $sumOfOcc[$key] = $alphasOcc[$key] + $alphasCapOcc[$key];
        }    
       
        return array_combine($alphas, $sumOfOcc);//Returns an associative array
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        $existingLetters = array_diff($parsedFile, [0]); //removing letters which occur 0 times
        asort($existingLetters); //Sort an array and maintain index association
 
        echo '<pre>'; print_r($existingLetters); echo '</pre>';
        $keys = array_keys($existingLetters); //keys are letters
        $medianLetter = $keys[round(count($keys)/2)-1];
        $occurrences = $existingLetters[$medianLetter];
        return $medianLetter;
       
    }
}

$rabbit = new WhiteRabbit();

$arrayToPrint = $rabbit->findMedianLetterInFile("/Applications/MAMP/htdocs/whiteRabbitTest/txt/text1.txt");
echo "The median letter for text1.txt is:".'<pre>'; print_r($arrayToPrint); echo '</pre>';
$arrayToPrint = $rabbit->findMedianLetterInFile("/Applications/MAMP/htdocs/whiteRabbitTest/txt/text2.txt");
echo "The median letter for text2.txt is:".'<pre>'; print_r($arrayToPrint); echo '</pre>';
$arrayToPrint = $rabbit->findMedianLetterInFile("/Applications/MAMP/htdocs/whiteRabbitTest/txt/text3.txt");
echo "The median letter for text3.txt is:".'<pre>'; print_r($arrayToPrint); echo '</pre>';
$arrayToPrint = $rabbit->findMedianLetterInFile("/Applications/MAMP/htdocs/whiteRabbitTest/txt/text4.txt");
echo "The median letter for text4.txt is:".'<pre>'; print_r($arrayToPrint); echo '</pre>';
$arrayToPrint = $rabbit->findMedianLetterInFile("/Applications/MAMP/htdocs/whiteRabbitTest/txt/text5.txt");
echo "The median letter for text5.txt is:".'<pre>'; print_r($arrayToPrint); echo '</pre>';